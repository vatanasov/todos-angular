import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

export enum TodoStatus {
  New = 'new', InProgress = 'in-progress', Finished = 'finished'
}

export interface Todo {
  id: string;
  text: string;
  status: TodoStatus;
  createdAt: Date
}

export interface TodoUpdateProperties {
  text?: string;
  status?: TodoStatus;
  createdAt?: Date
}

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  private data$: BehaviorSubject<Todo[]> = new BehaviorSubject([]);
  
  public get todos$(): Observable<Todo[]> {
    return this.data$ as Observable<Todo[]> ; 
  } 

  constructor() { }

  create(text: string): Observable<Todo> {
    const id = `${Date.now()}`;
    const todo  = {
      id,
      text,
      createdAt: new Date(),
      status: TodoStatus.New
    }

    this.data$.next(this.data$.getValue().concat(todo));

    return of(todo);
  }

  read(id: string): Observable<Todo | undefined> {
    const value = this.data$.getValue().find(v => v.id === id);
    return of(value);
  }

  update(id: string, values: TodoUpdateProperties): Observable<Todo | undefined> {
    const todos = this.data$.getValue();
    const index = todos.findIndex(v => v.id === id);
    if (index > - 1) {
      const data = this.data$.getValue()[index];
      for (let key in values) {
        data[key] = values[key];
      }

      const updated = Object.assign({}, data, values);
      todos[index] = updated;
      this.data$.next(todos);
      return of(updated);
    }

    return of(undefined);
  }

  delete(id: string): Observable<boolean> {
    const todos = this.data$.getValue();
    const index = todos.findIndex(v => v.id === id);
    if (index > - 1) {
      todos.splice(index, 1);
      this.data$.next(todos);
      return of(true);
    }

    return of(false);
  }
}
