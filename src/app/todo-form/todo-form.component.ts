import { Component, Input, OnChanges, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Todo, TodoStatus, TodoUpdateProperties } from '../todos.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnChanges {
  @Input()
  public todo: TodoUpdateProperties;

  @Output()
  onSave: EventEmitter<TodoUpdateProperties> = new EventEmitter();

  public form: FormGroup = this.fb.group({
    'text': ['', Validators.required],
    'status': [TodoStatus.New, Validators.required]
  });

  TodoStatus = TodoStatus;

  constructor(private fb: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    if ('todo' in changes && changes.todo.currentValue) {
      this.form.patchValue({
        text: changes.todo.currentValue.text,
        status: changes.todo.currentValue.status
      });
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.onSave.emit(this.form.value);
      this.form.patchValue({
        text: '',
        status: TodoStatus.New
      });
    }
  }

}
