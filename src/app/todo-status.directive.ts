import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TodoStatus } from './todos.service';

@Directive({
  selector: '[appTodoStatus]'
})
export class TodoStatusDirective implements OnChanges {

  @Input()
  public status: TodoStatus;

  constructor(private elementRef: ElementRef) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.status) {
      this.applyStyles();
    }
  }

  applyStyles() {
    switch (this.status) {
      case TodoStatus.InProgress:
        this.elementRef.nativeElement.style.border = '2px solid orange';
        break;
      case TodoStatus.Finished:
        this.elementRef.nativeElement.style.textDecoration = 'line-through';
        break;
      case TodoStatus.New:
        this.elementRef.nativeElement.style.border = '2px solid blue';
        break;
    }
  }

}
