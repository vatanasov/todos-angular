import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Todo, TodosService, TodoUpdateProperties } from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit, OnDestroy {

  public todos: Todo[] = [];

  public current: Todo;

  private subscription: Subscription = new Subscription();

  constructor(private todosService: TodosService) { }

  ngOnInit(): void {
    this.subscription.add(
      this.todosService.todos$.subscribe(v => this.todos = v)
      );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSelect(todo: Todo) {
    this.current = todo;
  }

  onSave(update: TodoUpdateProperties): void {
    if (this.current) {
      this.subscription.add(
        this.todosService.update(this.current.id, update).subscribe()
      );
      delete this.current;
    } else {
      this.subscription.add(
        this.todosService.create(update.text).subscribe()
      );
    }
  }
}
