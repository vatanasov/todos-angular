import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Todo } from '../todos.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  @Input()
  public data: Todo[] = [];

  @Input()
  public selected: Todo;

  @Output()
  public onSelect: EventEmitter<Todo> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onItemClick(todo: Todo): void {
    this.onSelect.emit(todo);
  }

}
